### **NOTICE: This project is dead.** ###

# README #
This is an `Auto/Visual Lisp` Snippet file for the SnipMate Vim Plugin.

### How do I get set up? ###
If you don't have a preferred installation method, I recommend installing [pathogen.vim](https://github.com/tpope/vim-pathogen), and then simply copy and paste:

    cd ~/.vim/bundle
    git clone https://JohnKaul@bitbucket.org/JohnKaul/alisp-snippets.git

Otherwise you can always download this and place the `lisp.snippet` file in the `snippets` directory. 

### Contribution guidelines ###

* Contribute? Please. Feel free.
* Code review? Yes, please.
* Comments? Yes, please.

### Git Standards ###
Each commit will be structured like this:
`"file name: Formatting cleanup."`
Where the file name is listed first followed by a colon and a brief description of the change.

### Coding Standards ###
1. Each time the file is updated a line in the file (`## File Last Updated: ` should also be updated. I use a simple vim mapping for this.

### Who do I talk to? ###

* John Kaul - john.kaul@outlook.com